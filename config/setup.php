<?php
	include 'database.php';

	try {
		$db = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
		// set the PDO error mode to exception
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "CREATE DATABASE IF NOT EXISTS camagru_paulk";
		// use exec() because no results are returned
		$db->exec($sql);
		$sql = "USE camagru_paulk;
				CREATE TABLE `users` (`id` int(32) NOT NULL AUTO_INCREMENT , PRIMARY KEY (`id`),`login` varchar(255) NOT NULL,`password` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, status VARCHAR(32)) ENGINE=MyISAM DEFAULT CHARSET=utf8;
				CREATE TABLE `photo` (`id` int(32) NOT NULL,`path` varchar(32) NOT NULL,`users_id` int(11) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=utf8;
				CREATE TABLE `comments` (`id` int(32) NOT NULL,`photo_id` int(11) NOT NULL,`users_id` int(11) NOT NULL,`comment` text) ENGINE=MyISAM DEFAULT CHARSET=utf8;
				CREATE TABLE `likes` (`photo_id` int(11) NOT NULL,`users_id` int(11) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
		$db->exec($sql);
	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
		die();
	}
