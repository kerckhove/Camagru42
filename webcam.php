<link rel="stylesheet" href="css/styles.css" media="screen" title="no title" charset="utf-8">
<?php
include_once 'includes/header.php';
?>
<div class="webcam">
  <video id="video"></video>
  <button class="btn" id="startbutton">Snap it</button>
  <canvas id="canvas"></canvas>
  <form action="#" method="post">
    <input type="text" style="display:none;" name="photo" id="photo" />
    <button type="sumbit" id="savebutton">Save it</button>
  </form>
  <form action="#" method="post" enctype="multipart/form-data">
  <input type="file" id="uploadphoto" name="upload"/>
  <button type="submit" id="savebutton2">Upload picture</button>
  </form>
  <script src="webcam.js"></script>
</div>
<?php
if (isset($_POST['photo']))
{
	$data = explode(',', $_POST['photo']);
  $date = date("d-m-Y_H:i:s");
	file_put_contents("photos/photo.$date.png", base64_decode($data[1]));
}

if (isset($_FILES['upload']))
{
  $date = date("d-m-Y_H:i:s");
  move_uploaded_file($_FILES['upload']['tmp_name'], "photos/photo.$date.png");
}
include_once 'includes/footer.php';
?>
