
<link rel="stylesheet" href="css/styles.css" media="screen" title="no title" charset="utf-8">
<?php
include_once 'includes/header.php';
 ?>
 <head>
   <body>
     <div class="myprofile"> <h1>Edit profile</h1></div> <br /> <br />
     <label for="first_name">First name</label>
     <input type="text" class="inputfirstname" /> <br /> <br />
     <label for="last_name">Last name</label>
     <input type="text" class="inputlastname" /> <br /> <br />
     <label for="email">Email</label>
     <input type="text" class="email" /> <br /> <br />
     <label for="oldpass">Old password</label>
     <input type="password" class="oldpass" /> <br /> <br />
     <label for="newpass">New password</label>
     <input type="password" class="newpassword" /> <br /> <br />
     <button class="btnaccount" id="startbutton">Save changes</button>
     <button class="btncancelaccount" id="startbutton">Cancel</button>
   </body>
 </head>
 <?php
include_once 'includes/footer.php';
?>
